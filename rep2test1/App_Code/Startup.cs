﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(rep2test.Startup))]
namespace rep2test
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
